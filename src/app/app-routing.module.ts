import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component';
import { VerSucursalesComponent } from './components/ver-sucursales/ver-sucursales.component';
import { VerProvedorComponent } from './components/ver-provedor/ver-provedor.component';
import { VerPersonalComponent } from './components/ver-personal/ver-personal.component';

const routes: Routes = [
    {
        path : 'Inicio/Productos',
        component : VerProductosComponent
    },
    {
        path : 'Inicio/Sucursales',
        component : VerSucursalesComponent
    },
    {
        path : 'Inicio/Provedores',
        component : VerProvedorComponent
    },
    {
        path : 'Inicio/Personal',
        component : VerPersonalComponent
    },
    {
        path : '',
        redirectTo : 'Inicio/Productos',
        pathMatch : 'full',

    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  })
  export class AppRoutingModule { }