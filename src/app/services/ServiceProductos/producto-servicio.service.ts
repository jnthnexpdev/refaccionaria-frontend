import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductoServicioService {

  public api = "http://localhost:3900";

  constructor( private _http:HttpClient) { 
  }

  obtenerProductos(){
    const url = `${this.api}/obtener/productos/`;
    console.log(url);
    return this._http.get(url);
  }
}