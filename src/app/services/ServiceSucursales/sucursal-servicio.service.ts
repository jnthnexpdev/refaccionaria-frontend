import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SucursalServicioService {

  public link = "http://localhost:3900"

  constructor(private _http : HttpClient) { }

  obtenerSucursales(){
    const url = `${this.link}/obtener/sucursal/`;
    console.log(url);
    return this._http.get(url);
  }
}
