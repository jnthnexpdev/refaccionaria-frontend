import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProvedorServicioService {

  public api1 = "http://localhost:3900";

  constructor(private _http : HttpClient) { }

  obtenerProvedor(){
    const url = `${this.api1}/obtener/provedores/`;
    console.log(url);
    return this._http.get(url);
  }
}
