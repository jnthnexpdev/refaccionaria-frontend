import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServicePersonalService {

  public api = "http://localhost:3900";

  constructor(private _http : HttpClient) { }

  obtenerPersonal(){
    const url = `${this.api}/obtener/empleados/`;
    console.log(url);
    return this._http.get(url);
  }
}
