import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'

import { AppComponent } from './app.component';
import { MenuBarComponent } from './components/menu-bar/menu-bar.component';
import { VerProductosComponent } from './components/ver-productos/ver-productos.component';
import { VerSucursalesComponent } from './components/ver-sucursales/ver-sucursales.component';
import { VerPersonalComponent } from './components/ver-personal/ver-personal.component';
import { VerProvedorComponent } from './components/ver-provedor/ver-provedor.component';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    MenuBarComponent,
    VerProductosComponent,
    VerSucursalesComponent,
    VerPersonalComponent,
    VerProvedorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
