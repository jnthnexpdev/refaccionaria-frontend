import { Component, OnInit } from '@angular/core';
import { ServicePersonalService } from 'src/app/services/ServicePersonal/service-personal.service';

@Component({
  selector: 'app-ver-personal',
  templateUrl: './ver-personal.component.html',
  styleUrls: ['./ver-personal.component.css']
})

export class VerPersonalComponent implements OnInit{

  datos : any[] = [];

  constructor(private _personal : ServicePersonalService){}

  ngOnInit(): void {
    this.cargarPersonal();
  }

  cargarPersonal(){
    this._personal.obtenerPersonal().subscribe((data : any) => {
      this.datos = data.empleados;
      console.log(this.datos);
    });
  }

}
