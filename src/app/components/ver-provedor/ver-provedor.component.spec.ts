import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerProvedorComponent } from './ver-provedor.component';

describe('VerProvedorComponent', () => {
  let component: VerProvedorComponent;
  let fixture: ComponentFixture<VerProvedorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerProvedorComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(VerProvedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
