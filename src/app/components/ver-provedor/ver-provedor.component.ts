import { Component, OnInit } from '@angular/core';
import { ProvedorServicioService } from 'src/app/services/ServiceProvedores/provedor-servicio.service';

@Component({
  selector: 'app-ver-provedor',
  templateUrl: './ver-provedor.component.html',
  styleUrls: ['./ver-provedor.component.css']
})
export class VerProvedorComponent implements OnInit{

  datos : any [] = [];

  constructor(private _provedor : ProvedorServicioService){}

  ngOnInit(): void {
    this.cargarProvedores()
  }

  cargarProvedores(){
    this._provedor.obtenerProvedor().subscribe((data : any) => {
    this.datos = data.provedores;
    console.log(this.datos);
    })
  }

}
