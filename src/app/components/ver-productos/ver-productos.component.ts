import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductoServicioService } from 'src/app/services/ServiceProductos/producto-servicio.service';


@Component({
  selector: 'app-ver-productos',
  templateUrl: './ver-productos.component.html',
  styleUrls: ['./ver-productos.component.css']
})
export class VerProductosComponent implements OnInit{

  datos: any[] = [];

  constructor(private _productos:ProductoServicioService, private router: Router){}

  ngOnInit(): void {
    this.cargarProductos();
  }

  cargarProductos(){
    this._productos.obtenerProductos().subscribe((data : any ) => {
      this.datos = data.productos;
      console.log(this.datos);
    })
  }

}
