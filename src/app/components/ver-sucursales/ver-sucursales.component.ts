import { Component, OnInit } from '@angular/core';
import { SucursalServicioService } from 'src/app/services/ServiceSucursales/sucursal-servicio.service';

@Component({
  selector: 'app-ver-sucursales',
  templateUrl: './ver-sucursales.component.html',
  styleUrls: ['./ver-sucursales.component.css']
})
export class VerSucursalesComponent implements OnInit{

  datos : any [] =[]

  constructor(private _sucursal : SucursalServicioService){}

  ngOnInit(): void {
    this.cargarSucursales();
  }

  cargarSucursales(){
    this._sucursal.obtenerSucursales().subscribe((data : any) => {
      this.datos = data.sucursal;
      console.log(this.datos);
      
    })
  }

}
